# What is an arthroscopy? #

Arthroscopy is a medical procedure done to see, analyze, and treat joint issues. It is a negligibly obtrusive medical procedure done on a short term premise. This implies that the patient can return home around the same time after the medical procedure.

Arthroscopy comes from the Greek words "arthro" (joint) and "skopien" (to look). Thus, arthroscopy implies investigating the joint.

In arthroscopy, a muscular specialist makes a little cut in the skin. The specialist then, at that point, embeds a smaller than usual instrument called an arthroscope. It assists with survey the inside of the joint. The medical procedure fixes joint harms.

Worn out on living with your sickness? Is your concern impeding your day to day existence assignments whether it be going to office or simply having a great time with your children?

What is an arthroscope?
An arthroscope is a cylinder like instrument that contains optical strands, a little focal point, and a camcorder to see the inside pieces of the joints. The light projected through the fiber optics enlightens the inside of the joint. The little camera takes the pictures and activities them on a TV screen. The specialist perspectives and treats the issue.

Arthroscopes are of various sizes. The size relies upon the joint analyzed. For e.g., for treating little joints, specialists use arthroscopes around 0.5 mm in size. For joints that are huge in size like knee joints, specialists can utilize a 5 mm long arthroscope.

Where is arthroscopy done?
Arthroscopy assists with diagnosing and treat the different pieces of the joint, for example,

Knee
Lower leg
Hip
Shoulder
Elbow
Wrist
Arthroscopy versus open a medical procedure
negligibly intrusive arthroscopic surgeryWhether the specialist will do open a medical procedure or arthroscopy relies upon the sort of joint issues or the injury. A couple of years back, conventional open a medical procedure was the main choice to treat joint issues. Arthroscopy is less obtrusive, utilizes a little cut, and shows continuous pictures of the inside of the joints.

Advantages of arthroscopy:
Little cut site
Less blood misfortune
Quicker recuperation
Restricted chances. look at a href="https://shoulderandkneeclinic.com/arthroscopy-specialist-mumbai/">best arthroscopic surgeon in mumbai</a>
Insignificant agony
Less requirement for torment drugs
Acted in the short term setting
Advantages of open a medical procedure
Used to treat extreme and complex wounds
More space to picture the injury
The most ideal for little and complex joints
More extensive degree to embed huge prosthetics
Until this point in time, numerous specialists lean toward open a medical procedure because of the restricted extent of arthroscopy.

Arthroscopic medical procedure is a decent decision for youthful competitors who need to get back to their calling and is likewise appropriate for elderly folks individuals who have depleted all careful choices for joint therapies. The decision of medical procedure relies upon the muscular specialist and the sort of infection.

Groundwork for arthroscopy medical procedure
Preoperative assessment
The specialist will play out specific indicative tests to plan the careful arrangement, for example,

A x-beam
CT (figured tomography) examine
Ultrasound of the delicate tissues
X-ray (attractive reverberation imaging) sweep of the delicate tissues
Blood tests to discover CRP (C-receptive protein), ESR (erythrocyte sedimentation rate), WBC (white platelet) count, RF (rheumatoid component), and so on
Urinalysis
The specialist may likewise play out a joint goal (arthrocentesis) methodology to eliminate some joint liquid with the assistance of a needle to detect anomalies.

Planning for the technique
Your primary care physician or medical care supplier will encourage you to make specific way of life changes. Stop specific exercises like games or thorough activities assuming you have injury or agony in the joints.
The careful group will disclose to you the medical procedure, aftercare, and likely dangers.
You will be approached to sign an assent structure upon the arrival of the medical procedure.
Upon the arrival of the activity, you need to bring your ID card and different reports. Check out <a href+"https://g.page/shoulderandkneespecialist">arthroscopy surgeon in mumbai</a>
The specialist will audit your prescriptions. A few meds will be ended like anti-inflamatory medicine, NSAIDs (nonsteroidal mitigating drugs), blood thinners, and so forth You should converse with your medical care supplier about different meds, natural enhancements, nutrients, remedy, or non-doctor prescribed prescriptions you are taking to keep away from intricacies post-medical procedure.
Contingent upon the kind of sedation, you will be approached to quit eating food sources or drinking fluids after 12 PM, the night preceding the medical procedure. There are by and large no food limitations for nearby sedation.
Ask a companion or a relative to go with you upon the arrival of the medical procedure.
Make plans for a ride home.
Method
arthroscopy

On the day you are having the medical procedure, you need to show up sooner than expected in the clinic or facility.
The medical caretaker will request that you change to a clinic outfit.
The specialist might apply neighborhood, general, or spinal sedation.
In the event that nearby sedation is applied, you will feel deadness in the joint and slight pulling sensation during the medical procedure.
The specialist will clean the region of the entry point site with an antibacterial liquid.
A little entry point will be made.
The arthroscope will be embedded through the cut site.
Extra cuts will be made to embed the test and different instruments.
A sterile arrangement will be put inside the joint for further developed vision.
The arthroscope will send pictures for investigation.
Restorative medical procedure will be done to eliminate harmed tissues.
After the medical procedure is finished, the liquid and the arthroscope will be taken out.
The entry point locales will be shut with join or tapes.
A sterile dressing will be applied.
After medical procedure
The patient is to the recuperation room. The attendant will screen the patient and actually look at the essential signs. After legitimate rest, the patient can return home. The attendant or the specialist will clarify the post-usable consideration and give torment prescriptions. Specialists will give directions with respect to joint versatility and the utilization of supports.

When would it be a good idea for you to go for an arthroscopy?
shoulder painTo analyze the degree of the harm, the specialist will inspect the patient. He/she will check the clinical history and request x-beams and other imaging concentrates on like a MRI or CT examine.

An arthroscopy assists with treating:

Irritation
For patients experiencing fiery joint pain or rheumatoid joint inflammation, arthroscopy eliminates the kindled inward covering of tissues (synovium) in the joints of the knees, shoulder, wrist, lower legs, and elbow. The tissue is biopsied to decide the reason for the irritation like joint pain or tuberculosis.

Non-provocative infections
Arthroscopy is utilized to analyze unpredictable ligaments in non-provocative infections like osteoarthritis.

आर्थोस्कोपी एक चिकित्सा प्रक्रिया है जो संयुक्त मुद्दों को देखने, विश्लेषण करने और उनका इलाज करने के लिए की जाती है ।  यह एक अल्पकालिक आधार पर किया एक लापरवाही निकला हुआ चिकित्सा प्रक्रिया है ।  इसका तात्पर्य यह है कि रोगी चिकित्सा प्रक्रिया के बाद उसी समय घर लौट सकता है । 

आर्थ्रोस्कोपी ग्रीक शब्द "आर्थ्रो" (संयुक्त) और "स्कोपियन" (देखने के लिए) से आता है ।  इस प्रकार, आर्थ्रोस्कोपी का अर्थ है संयुक्त की जांच करना । 

आर्थोस्कोपी में, एक पेशी विशेषज्ञ त्वचा में थोड़ा कटौती करता है ।  विशेषज्ञ, उस बिंदु पर, आर्थ्रोस्कोप नामक सामान्य उपकरण की तुलना में एक छोटा एम्बेड करता है ।  यह संयुक्त के अंदर सर्वेक्षण के साथ सहायता करता है ।  चिकित्सा प्रक्रिया संयुक्त हानि को ठीक करती है । 

अपनी बीमारी के साथ रहने पर बाहर पहना? क्या आपकी चिंता आपके दिन-प्रतिदिन के अस्तित्व के असाइनमेंट को बाधित कर रही है, चाहे वह कार्यालय जा रहा हो या बस अपने बच्चों के साथ एक अच्छा समय बिता रहा हो?

क्या है एक arthroscope?
एक आर्थ्रोस्कोप एक सिलेंडर जैसा उपकरण होता है जिसमें ऑप्टिकल स्ट्रैंड्स, थोड़ा फोकल पॉइंट और जोड़ों के अंदर के टुकड़ों को देखने के लिए एक कैमकॉर्डर होता है ।  फाइबर ऑप्टिक्स के माध्यम से प्रक्षेपित प्रकाश संयुक्त के अंदर को प्रबुद्ध करता है ।  थोड़ा कैमरा एक टीवी स्क्रीन पर तस्वीरें और गतिविधियों उन्हें लेता है. विशेषज्ञ दृष्टिकोण और मुद्दे का इलाज करता है । 

आर्थ्रोस्कोप विभिन्न आकारों के होते हैं ।  आकार संयुक्त विश्लेषण पर निर्भर करता है ।  उदाहरण के लिए, छोटे जोड़ों के इलाज के लिए, विशेषज्ञ आकार में लगभग 0.5 मिमी आर्थ्रोस्कोप का उपयोग करते हैं ।  जोड़ों के लिए जो घुटने के जोड़ों की तरह आकार में विशाल हैं, विशेषज्ञ 5 मिमी लंबे आर्थ्रोस्कोप का उपयोग कर सकते हैं । 

आर्थोस्कोपी कहाँ किया जाता है?
आर्थ्रोस्कोपी संयुक्त के विभिन्न टुकड़ों के निदान और उपचार के साथ सहायता करता है, उदाहरण के लिए,

घुटने
निचला पैर
हिप
कंधे
कोहनी
कलाई
आर्थोस्कोपी बनाम एक चिकित्सा प्रक्रिया खोलें
लापरवाही से घुसपैठ आर्थ्रोस्कोपिक सर्जरीक्या विशेषज्ञ एक चिकित्सा प्रक्रिया खोलेगा या आर्थ्रोस्कोपी संयुक्त मुद्दों या चोट के प्रकार पर निर्भर करता है ।  कुछ साल पहले, पारंपरिक खुले एक चिकित्सा प्रक्रिया संयुक्त मुद्दों के इलाज के लिए मुख्य विकल्प था ।  आर्थ्रोस्कोपी कम निकला हुआ है, थोड़ा कटौती का इस्तेमाल करता है, और जोड़ों के अंदर की निरंतर तस्वीरें दिखाता है । 

आर्थोस्कोपी के लाभ:
लिटिल कट साइट
कम रक्त दुर्भाग्य
जल्दी स्वस्थ हो जाना
प्रतिबंधित संभावना
तुच्छ पीड़ा
पीड़ा दवाओं के लिए कम आवश्यकता
शॉर्ट टर्म सेटिंग में काम किया
एक चिकित्सा प्रक्रिया खोलने के लाभ
चरम और जटिल घावों का इलाज करने के लिए उपयोग किया जाता है
चोट की तस्वीर के लिए अधिक स्थान
छोटे और जटिल जोड़ों के लिए सबसे आदर्श
विशाल प्रोस्थेटिक्स को एम्बेड करने के लिए अधिक व्यापक डिग्री
इस समय तक, कई विशेषज्ञ आर्थोस्कोपी की प्रतिबंधित सीमा के कारण एक चिकित्सा प्रक्रिया खोलने की ओर झुकते हैं । 

आर्थोस्कोपिक चिकित्सा प्रक्रिया युवा प्रतियोगियों के लिए एक सभ्य निर्णय है, जिन्हें अपनी कॉलिंग पर वापस जाने की आवश्यकता होती है और इसी तरह बुजुर्ग लोगों के लिए उपयुक्त है, जिन्होंने संयुक्त उपचारों के लिए सभी सावधान विकल्पों को समाप्त कर दिया है ।  चिकित्सा प्रक्रिया का निर्णय मांसपेशियों के विशेषज्ञ और संक्रमण के प्रकार पर निर्भर करता है । 

आर्थोस्कोपी चिकित्सा प्रक्रिया के लिए आधार
Preoperative मूल्यांकन
विशेषज्ञ उदाहरण के लिए सावधानीपूर्वक व्यवस्था की योजना बनाने के लिए विशिष्ट संकेतक परीक्षण खेलेंगे,

एक एक्स-बीम
CT (लगा टोमोग्राफी) की जांच
नाजुक ऊतकों का अल्ट्रासाउंड
नाजुक ऊतकों का एक्स-रे (आकर्षक पुनर्संयोजन इमेजिंग) स्वीप
सीआरपी (सी-ग्रहणशील प्रोटीन), ईएसआर (एरिथ्रोसाइट अवसादन दर), डब्ल्यूबीसी (सफेद प्लेटलेट) गिनती, आरएफ (संधिशोथ घटक), और इसी तरह की खोज करने के लिए रक्त परीक्षण
Urinalysis
विशेषज्ञ इसी तरह विसंगतियों का पता लगाने के लिए सुई की सहायता से कुछ संयुक्त तरल को खत्म करने के लिए एक संयुक्त लक्ष्य (आर्थ्रोसेंटेसिस) पद्धति खेल सकता है । 

तकनीक के लिए योजना
आपका प्राथमिक देखभाल चिकित्सक या चिकित्सा देखभाल आपूर्तिकर्ता आपको जीवन परिवर्तन के विशिष्ट तरीके बनाने के लिए प्रोत्साहित करेगा ।  खेल या गहन गतिविधियों जैसे विशिष्ट अभ्यासों को रोकें, यह मानते हुए कि आपको जोड़ों में चोट या पीड़ा है । 
सावधान समूह आपको चिकित्सा प्रक्रिया, आफ्टरकेयर और संभावित खतरों का खुलासा करेगा । 
चिकित्सा प्रक्रिया के आगमन पर आपको एक सहमति संरचना पर हस्ताक्षर करने के लिए संपर्क किया जाएगा । 
गतिविधि के आगमन पर, आपको अपना आईडी कार्ड और विभिन्न रिपोर्ट लाने की आवश्यकता है । 
विशेषज्ञ आपके नुस्खे का ऑडिट करेगा। कुछ meds समाप्त हो जाएगा की तरह विरोधी inflamatory चिकित्सा, एनएसएआईडी (nonsteroidal को कम करने) दवाओं, रक्त को पतला, और इतना आगे आप के साथ बातचीत करना चाहिए अपने चिकित्सा देखभाल आपूर्तिकर्ता के बारे में अलग अलग meds, प्राकृतिक संवर्द्धन, पोषक तत्वों, उपाय, या गैर-चिकित्सक निर्धारित नुस्खे आपको ले जा रहे हैं से दूर रखने के लिए जटिलताओं के बाद चिकित्सा प्रक्रिया है.
बेहोश करने की क्रिया की तरह पर आकस्मिक, आप भोजन स्रोतों खाने या पीने के तरल पदार्थ 12 बजे के बाद, चिकित्सा प्रक्रिया से पहले रात छोड़ने के लिए संपर्क किया जाएगा ।  पास के बेहोश करने की क्रिया के लिए भोजन की कोई सीमा नहीं है । 
चिकित्सा प्रक्रिया के आगमन पर किसी साथी या रिश्तेदार को अपने साथ जाने के लिए कहें । 
एक सवारी घर के लिए योजना बनाएं । 
विधि
आर्थोस्कोपी

जिस दिन आप चिकित्सा प्रक्रिया कर रहे हैं, आपको क्लिनिक या सुविधा में अपेक्षा से अधिक जल्दी दिखाना होगा । 
चिकित्सा कार्यवाहक अनुरोध करेगा कि आप एक क्लिनिक संगठन में बदल जाएं । 
विशेषज्ञ पड़ोस, सामान्य, या रीढ़ की हड्डी के बेहोश करने की क्रिया को लागू कर सकता है । 
इस घटना में कि पास में बेहोश करने की क्रिया लागू होती है, आप चिकित्सा प्रक्रिया के दौरान संयुक्त और मामूली खींचने वाली सनसनी में मृत्यु महसूस करेंगे । 
विशेषज्ञ एक जीवाणुरोधी तरल के साथ प्रवेश बिंदु साइट के क्षेत्र को साफ करेगा । 
थोड़ा प्रवेश बिंदु बनाया जाएगा । 
आर्थ्रोस्कोप को कट साइट के माध्यम से एम्बेड किया जाएगा । 
परीक्षण और विभिन्न उपकरणों को एम्बेड करने के लिए अतिरिक्त कटौती की जाएगी । 
आगे विकसित दृष्टि के लिए संयुक्त के अंदर एक बाँझ व्यवस्था रखी जाएगी । 
आर्थोस्कोप जांच के लिए तस्वीरें भेजेगा।
नुकसान पहुंचाने वाले ऊतकों को खत्म करने के लिए पुनर्स्थापनात्मक चिकित्सा प्रक्रिया की जाएगी । 
चिकित्सा प्रक्रिया समाप्त होने के बाद, तरल और आर्थ्रोस्कोप को बाहर निकाला जाएगा । 
एंट्री पॉइंट लोकेशन को जॉइन या टेप के साथ बंद कर दिया जाएगा । 
एक बाँझ ड्रेसिंग लागू किया जाएगा । 
चिकित्सा प्रक्रिया के बाद
रोगी आरोग्यलाभ कक्ष में है। अटेंडेंट मरीज को स्क्रीन करेगा और वास्तव में आवश्यक संकेतों को देखेगा ।  वैध आराम के बाद, रोगी घर लौट सकता है ।  परिचर या विशेषज्ञ पोस्ट-प्रयोग करने योग्य विचार को स्पष्ट करेंगे और पीड़ा के नुस्खे देंगे ।  विशेषज्ञ संयुक्त बहुमुखी प्रतिभा और समर्थन के उपयोग के संबंध में दिशा-निर्देश देंगे । 

आर्थोस्कोपी के लिए जाना आपके लिए एक अच्छा विचार कब होगा?
कंधे का दर्दनुकसान की डिग्री का विश्लेषण करने के लिए, विशेषज्ञ रोगी का निरीक्षण करेगा ।  वह/वह जाँच करेगा नैदानिक इतिहास और अनुरोध x-मुस्कराते हुए और अन्य इमेजिंग पर ध्यान केंद्रित की तरह एक एमआरआई या सीटी की जांच.

एक आर्थ्रोस्कोपी उपचार के साथ सहायता करता है:

जलन
उग्र जोड़ों के दर्द या संधिशोथ संयुक्त सूजन का अनुभव करने वाले रोगियों के लिए, आर्थ्रोस्कोपी घुटनों, कंधे, कलाई, निचले पैरों और कोहनी के जोड़ों में ऊतकों (सिनोवियम) के अंदर के आवरण को समाप्त करता है ।  जोड़ों के दर्द या तपेदिक जैसी जलन का कारण तय करने के लिए ऊतक को बायोप्सी किया जाता है । 

गैर उत्तेजक संक्रमण
आर्थ्रोस्कोपी का उपयोग ऑस्टियोआर्थराइटिस जैसे गैर-उत्तेजक संक्रमणों में अप्रत्याशित स्नायुबंधन का विश्लेषण करने के लिए किया जाता है । 